from django.db import models

# Create your models here.

class ShowMatkul(models.Model):
    Nama_Mata_Kuliah        = models.CharField(max_length=50)
    Dosen_Pengajar          = models.CharField(max_length=50)
    Jumlah_SKS              = models.CharField(max_length=50)
    Deskripsi_Mata_Kuliah   = models.CharField(max_length=50)
    Semester_Tahun          = models.CharField(max_length=50)
    Ruang_Kelas             = models.CharField(max_length=50)

    def __str__(self):
        return "{}. {}".format(self.id , self.Nama_Mata_Kuliah)
