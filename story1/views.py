from django.shortcuts import render
from django.http import HttpResponseRedirect

#Import from
from .forms import MataKuliah
from .models import ShowMatkul
# Create your views here.

def index(request):
    return render(request, 'story1.html')

def home(request):
    return render(request,'homepage.html')

def profile(request):
    return render(request, 'profileisland.html')

def academic(request):
    return render(request, 'academicisland.html')

def achievement(request):
    return render(request, 'achievementisland.html')

def cv(request):
    return render(request, 'cvisland.html')

def isolated(request):
    return render(request, 'isolatedisland.html')

def matakuliahCreate(request):
    Mata_Kuliah = MataKuliah()

    if request.method == 'POST':
        ShowMatkul.objects.create(
            Nama_Mata_Kuliah        = request.POST['Nama_Mata_Kuliah'],
            Dosen_Pengajar          = request.POST['Dosen_Pengajar'],
            Jumlah_SKS              = request.POST['Jumlah_SKS'],
            Deskripsi_Mata_Kuliah   = request.POST['Deskripsi_Mata_Kuliah'],
            Semester_Tahun          = request.POST['Semester_Tahun'],
            Ruang_Kelas             = request.POST['Ruang_Kelas'],
        )

        return HttpResponseRedirect("/AcademicIsland/story5/MataKuliah")

    context = {
        'MataKuliah':Mata_Kuliah
    }
    
    return render(request, 'matakuliahCreate.html',context)

def matakuliahShow(request):
    Matkul = ShowMatkul.objects.all()
    context = {
        'MataKuliah':'Mata Kuliah',
        'Matkul': Matkul
    }
    if request.method == 'POST':
        print("apakah berhasil")
    return render(request, 'matakuliahShow.html',context)

def matakuliahDelete(request,pk):
    deleteMatkul = ShowMatkul.objects.filter(pk=pk)
    deleteMatkul.delete()

    return HttpResponseRedirect("/AcademicIsland/story5/MataKuliah")

def matakuliahDetails(request,pk):
    detailsMatkul = ShowMatkul.objects.get(pk=pk)
    context = {
        'list' : detailsMatkul
    }

    return render(request, 'matakuliahDetails.html',context)


def uts(request):
    return render(request,'nyoba.html')