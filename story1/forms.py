from django import forms


class MataKuliah(forms.Form):
    Nama_Mata_Kuliah        = forms.CharField()
    Dosen_Pengajar          = forms.CharField()
    Jumlah_SKS              = forms.CharField()
    Deskripsi_Mata_Kuliah   = forms.CharField()
    Semester_Tahun          = forms.CharField()
    Ruang_Kelas             = forms.CharField()