from django.urls import path

from . import views

app_name = 'story1'

urlpatterns = [
    path('AcademicIsland/story1/', views.index, name='index'),
    path('AcademicIsland/story5/', views.matakuliahCreate, name='matakuliah'),

    path('',views.home, name="home"),
    path('ProfileIsland/',views.profile, name="profile"),
    path('AcademicIsland/',views.academic, name="academic"),
    path('AchievementIsland/',views.achievement, name="achievement"),
    path('CVIsland/',views.cv, name="cv"),
    path('IsolatedIsland/',views.isolated, name="isolated"),
    path('AcademicIsland/story5/MataKuliah', views.matakuliahShow, name='matakuliah'),
    path("AcademicIsland/story5/MataKuliah/<int:pk>", views.matakuliahDelete, name="matakuliahDelete"),
    path("AcademicIsland/story5/MataKuliah/no<int:pk>", views.matakuliahDetails, name="matakuliahDetails"),
    path("uts/", views.uts, name="uts"),
]