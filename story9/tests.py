from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import loginpage, register, logoutpage, message
from django.contrib.auth.models import User


# Create your tests here.
from .apps import Story9Config


class TestPages(TestCase):
    def test_url_story9(self):
        response = Client().get('/story9/')
        self.assertEquals(200, response.status_code)

    def test_url_story9(self):
        response = Client().get('/story9/hello/')
        self.assertEquals(302, response.status_code)

    def test_app_name(self):
        self.assertEqual(Story9Config.name, "story9")

# Register
class TestRegister(TestCase):
    def test_url_story9(self):
        response = Client().get('/story9/register/')
        self.assertEquals(200, response.status_code)

    def test_event_func(self):
        found_func = resolve('/story9/register/')
        self.assertEqual(found_func.func, register)


# LOG OUT
class TestLogOut(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story9/logout/')
        self.assertEqual(response.status_code, 302)

    def test_event_func(self):
        found_func = resolve('/story9/logout/')
        self.assertEqual(found_func.func, logoutpage)


# LOG IN
class TestLogIn(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/story9/')
        self.assertEqual(found_func.func, loginpage)

    def test_event_using_template(self):
        template = Client().get('/story9/')
        self.assertTemplateUsed(template, 'login.html')
