from django.db import models

# Create your models here.
# class Pdf(models.Model):
#     f = open('/path/to/hello.world')
#     myfile = File(f)
#     Nama = models.FileFeld()

class SuratPemerintah(models.Model):
    Jenis = models.CharField(max_length=100)
    Judul = models.CharField(max_length=150)
    Tentang = models.CharField(max_length=500)
    pdf = models.FileField(upload_to='SuratPemerintah/pdfs/')

    def __str__(self):
        return "{}. {} : {}".format(self.id , self.Jenis ,self.Judul)
    