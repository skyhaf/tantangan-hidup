from django.urls import path

from . import views

app_name = 'tk1'

urlpatterns = [
    path('',views.home, name="home"),
    path('perpu/',views.perpu, name="perpu"),
    path('uu/',views.uu, name="uu"),
    path('pp/',views.pp, name="pp"),
    path('perpres/',views.perpres, name="perpres"),
    path('perda/',views.perda, name="perda"),
    path('pergub/',views.pergub, name="pergub"),

]