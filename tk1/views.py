from django.shortcuts import render,  redirect
from django.views.generic import TemplateView, ListView, CreateView
from django.core.files.storage import FileSystemStorage
from django.urls import reverse_lazy

from .forms import SuratPemerintahForm
from .models import SuratPemerintah

# Create your views here.

def home(request):
    #form = SuratPemerintah()

    if request.method == 'POST':
        form = SuratPemerintahForm(request.POST, request.FILES)
        form.save()
        #return redirect('book_list')
    else:
        form = SuratPemerintahForm()

    return render(request, 'home.html',{'form':form})

    # if request.method == 'POST':
    #     form = BookForm(request.POST, request.FILES)
    #     if form.is_valid():
    #         form.save()
    #         return redirect('book_list')
    # else:
    #     form = BookForm()
    # return render(request, 'upload_book.html', {
    #     'form': form
    # })


def perpu(request):
    return render(request, 'pdf1.html')

def uu(request):
    return render(request, 'pdf2.html')

def pp(request):
    return render(request, 'pdf3.html')

def perpres(request):
    return render(request, 'pdf4.html')

def perda(request):
    return render(request, 'pdf5.html')

def pergub(request):
    return render(request, 'pdf6.html')