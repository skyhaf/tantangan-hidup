from django import forms

from .models import SuratPemerintah

Pilihan_Jenis = [
    ('(PERPU)' ,'Peraturan Pemerintah Pengganti Undang-Undang (PERPU)'),
    ('(UU)' ,'Undang-Undang (UU)'),
    ('(PP)' ,'Peraturan Pemerintah (PP)'),
    ('(PERPRES)' ,'Peraturan Presiden (PERPRES)'),
    ('(PERDA)' ,'Peraturan Daerah (PERDA)'),
    ('(PERGUB)' ,'Peraturan Gubernur (PERGUB)'),
]


class SuratPemerintahForm(forms.ModelForm):
    Jenis = forms.ChoiceField( choices=Pilihan_Jenis)

    Judul = forms.CharField()
    Tentang = forms.CharField(widget=forms.Textarea)
    pdf = forms.FileField()

    class Meta:
        model = SuratPemerintah
        fields = ('Jenis', 'Judul', 'Tentang','pdf')